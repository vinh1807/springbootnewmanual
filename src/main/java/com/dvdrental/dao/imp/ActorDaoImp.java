package com.dvdrental.dao.imp;

import java.util.List;

import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.dvdrental.dao.ActorDao;
import com.dvdrental.entity.Actor;

@Repository
public class ActorDaoImp implements ActorDao {

	private SessionFactory sessionFactory = SessionFactoryBuilder.getSessionFactory();

	public List<Actor> getAllActors() {
		Session session = sessionFactory.getCurrentSession();
		session.beginTransaction();
		@SuppressWarnings("unchecked")
		TypedQuery<Actor> query = session
				.createQuery("from Actor actor order by actor.actorId");
		return query.getResultList();
	}

	public List<Actor> getAllActorsByPage(String orderBy, int page, int pageSize) {
		Session session = sessionFactory.getCurrentSession();
		session.beginTransaction();
		@SuppressWarnings("unchecked")
		TypedQuery<Actor> query = session
				.createQuery("from Actor actor " + "order by actor." + orderBy);
		query.setFirstResult((page * pageSize) - pageSize);
		query.setMaxResults(pageSize);
		return query.getResultList();
	}

	public Actor getActorById(int id) {
		Session session = sessionFactory.getCurrentSession();
		session.beginTransaction();
		Actor actor = session.find(Actor.class, id);
		return actor;
	}

}
