package com.dvdrental.service.imp;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dvdrental.dao.ActorDao;
import com.dvdrental.dao.imp.ActorDaoImp;
import com.dvdrental.entity.Actor;
import com.dvdrental.service.ActorService;

@Service
public class ActorServiceImp implements ActorService {

	ActorDao actorDao = new ActorDaoImp();

	@Transactional
	public List<Actor> getAllActors() {
		return actorDao.getAllActors();
	}

	@Transactional
	public List<Actor> getAllActorsByPage(String orderBy, int page, int pageSize) {
		return actorDao.getAllActorsByPage(orderBy, page, pageSize);
	}

	@Transactional
	public Actor getActorById(int id) {
		return actorDao.getActorById(id);
	}

}
