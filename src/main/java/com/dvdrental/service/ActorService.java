package com.dvdrental.service;

import java.util.List;

import com.dvdrental.entity.Actor;

public interface ActorService {
	
	List<Actor> 	getAllActors();

	List<Actor>		getAllActorsByPage(String orderBy, int page, int pageSize);
	
	Actor			getActorById(int id);
	
}
